﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalVar
{
    public static string GAME_WORLD_SCENE = "GameWorld";
    public static string DEAD_ZONE = "DeadZone";
    public static string SCORE_ZONE = "ScoreZone";
}
