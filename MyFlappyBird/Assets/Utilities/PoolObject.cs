﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObject
{
    private Transform trans;
    private bool inUse;

    public Transform Trans { get => trans; }
    public bool InUse { get => inUse; }

    public PoolObject(Transform t)
    {
        this.trans = t;
    }

    public void Use()
    {
        inUse = true;
    }

    public void Dispose()
    {
        inUse = false;
    }
}
