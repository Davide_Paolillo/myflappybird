﻿enum PageState
{
    None,
    Start,
    GameOver,
    Countdown
}
