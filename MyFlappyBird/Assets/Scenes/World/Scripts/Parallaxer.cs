﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallaxer : MonoBehaviour
{
    [System.Serializable] private struct YSpawnRange
    {
        [SerializeField] public float min;
        [SerializeField] public float max;
    }

    [SerializeField] private GameObject prefab;
    [SerializeField] int poolSize;
    [SerializeField] float shiftSpeed;
    [SerializeField] float spawnRate;
    [SerializeField] private YSpawnRange ySpawnRange;
    [SerializeField] Vector3 defaultSpawnPos;
    [SerializeField] private bool spawnImmediate; // particle prewarm
    [SerializeField] private Vector3 immediateSpawnPos;
    [SerializeField] private Vector3 targetAspectRatio; // Aspect of the device where the game is actually playing

    private float spawnTime;
    private float targetAspect;
    private PoolObject[] poolObjects;
    GameManager game;
    private GameObject[] objInScene;

    private void Awake()
    {
        poolObjects = new PoolObject[poolSize];
        objInScene = new GameObject[poolSize];
    }

    // Start is called before the first frame update
    void Start()
    {
        game = GameManager.Instance;
        Configure();
    }

    private void OnEnable()
    {
        GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
    }

    private void OnDisable()
    {
        GameManager.OnGameOverConfirmed -= OnGameOverConfirmed;
    }

    // Update is called once per frame
    void Update()
    {
        if (!game.GameOver)
        {
            Shift();
            spawnTime += Time.deltaTime;
            if (spawnTime > spawnRate)
            {
                Spawn();
                spawnTime = 0;
            }
        }
    }

    private void OnGameOverConfirmed()
    {
        foreach (PoolObject poolObj in poolObjects)
        {
            poolObj.Dispose();
            poolObj.Trans.position = Vector3.one * 1000;
        }
        ClearScene();
        Configure();
    }

    private void ClearScene()
    {
        for (int i = 0; i < objInScene.Length; i++)
        {
            Destroy(objInScene[i]);
        }
    }

    private void Configure()
    {
        targetAspect = targetAspectRatio.x / targetAspectRatio.y;
        //poolObjects = new PoolObject[poolSize];
        for (int i = 0; i < poolObjects.Length; i++)
        {
            GameObject go = Instantiate(prefab) as GameObject;
            objInScene[i] = go;
            Transform t = go.transform;
            t.SetParent(transform);
            t.position = Vector3.one * 1000;
            poolObjects[i] = new PoolObject(t);
        }

        if (spawnImmediate)
        {
            SpawnImmediate();
        }
    }

    private void Spawn()
    {
        Transform t = GetPoolObject();
        if(t != null) // if t is null, so the pool object size is too small
        {
            Vector3 pos = Vector3.zero;
            pos.x = (defaultSpawnPos.x * Camera.main.aspect) / targetAspect; // Remains the same if camera aspect ratio is equal to target aspect, but if they are different the value will change
            pos.y = UnityEngine.Random.Range(ySpawnRange.min, ySpawnRange.max); // Setting the y of our game obj randomly in a range
            t.position = pos;
        }
        
    }
    
    // Some obj needs to spawn off screen immediatly, so we don't have a gap (of clouds for example) in the screen, so we immediatly spawn the second obj to appear
    private void SpawnImmediate()
    {
        Transform t = GetPoolObject();
        if (t != null) // if t is null, so the pool object size is too small
        {
            Vector3 pos = Vector3.zero;
            pos.x = (immediateSpawnPos.x * Camera.main.aspect) / targetAspect;
            pos.y = UnityEngine.Random.Range(ySpawnRange.min, ySpawnRange.max); // Setting the y of our game obj randomly in a range
            t.position = pos;
            Spawn();
        }
    }

    private void Shift()
    {
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObjects[i].Trans.position += Vector3.left * shiftSpeed * Time.deltaTime; // Movement to the left at shift speed
            CheckDisposeObject(poolObjects[i]);
        }
    }

    private void CheckDisposeObject(PoolObject poolObject)
    {
        if(poolObject.Trans.position.x < (-defaultSpawnPos.x * Camera.main.aspect) / targetAspect) // Cos we are spwaning things out of screen
        {
            poolObject.Dispose();
            poolObject.Trans.position = Vector3.one * 1000; // Set the position way out of screen
            foreach (GameObject go in objInScene)
            {
                go.GetComponentInChildren<Collider2D>().enabled = true;
            }
        }
    }

    private Transform GetPoolObject()
    {
        foreach (PoolObject poolObj in poolObjects)
        {
            if (!poolObj.InUse)
            {
                poolObj.Use();
                return poolObj.Trans;
            }
        }
        return null;
    }
}
