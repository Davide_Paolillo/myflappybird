﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TapController : MonoBehaviour
{
    public delegate void PlayerDelegate();
    public static event PlayerDelegate OnPlayerDied;
    public static event PlayerDelegate OnPlayerScored;

    [SerializeField] private float tapForce = 10.0f;
    [SerializeField] private float tiltSmooth = 5.0f;
    [SerializeField] private Collider2D FirstCollider;
    [SerializeField] private AudioClip tap;
    [SerializeField] private AudioClip death;
    [SerializeField] private AudioClip scored;

    private Vector3 startPos;
    private Rigidbody2D rigidbody;
    private Quaternion downRotation;
    private Quaternion forwardRotation;

    private GameManager game;
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        startPos = new Vector3(-2.0f, 0.5f, 0.0f);
        rigidbody = GetComponent<Rigidbody2D>();
        downRotation = Quaternion.Euler(0.0f, 0.0f, -90.0f);
        forwardRotation = Quaternion.Euler(0.0f, 0.0f, 35.0f);
        game = GameManager.Instance;

        if (game.GameOver)
        {
            rigidbody.simulated = false;
            transform.rotation = Quaternion.identity;
        }
    }

    private void OnEnable()
    {
        GameManager.OnGameStarted += OnGameStarted;
        GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
    }

    private void OnDisable()
    {
        GameManager.OnGameStarted -= OnGameStarted;
        GameManager.OnGameOverConfirmed -= OnGameOverConfirmed;
    }

    // Update is called once per frame
    void Update()
    {
        if (!game.GameOver)
        {
            if (Input.GetMouseButtonDown(0))
            {
                audioSource.PlayOneShot(tap);
                this.transform.rotation = forwardRotation;
                rigidbody.velocity = Vector3.zero;
                this.rigidbody.AddForce(Vector2.up * tapForce, ForceMode2D.Force);
            }

            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, downRotation, tiltSmooth * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.SCORE_ZONE))
        {
            collision.gameObject.GetComponent<Collider2D>().enabled = false;
            audioSource.PlayOneShot(scored);
            OnPlayerScored(); // event sent to GameManager
        } else if (collision.gameObject.tag.Equals(GlobalVar.DEAD_ZONE))
        {
            rigidbody.simulated = false;
            audioSource.PlayOneShot(death);
            OnPlayerDied(); // event sent to GameManager
        }
    }

    private void OnGameOverConfirmed()
    {
        transform.localPosition = startPos;
        transform.rotation = Quaternion.identity; // Rotazione identita', quindi 0 0 0 0
    }

    private void OnGameStarted()
    {
        rigidbody.velocity = Vector3.zero; // Reset the gravity avoiding our player to fall too fast to the ground
        rigidbody.simulated = true;
    }
}
