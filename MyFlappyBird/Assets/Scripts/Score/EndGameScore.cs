﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class EndGameScore : MonoBehaviour
{
    private Text score;

    void OnEnable()
    {
        score = GetComponent<Text>();
        score.text = "Game Over!";
    }
}
