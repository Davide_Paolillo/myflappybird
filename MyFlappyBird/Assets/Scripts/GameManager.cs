﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public delegate void GameDelegate();
    public static event GameDelegate OnGameStarted;
    public static event GameDelegate OnGameOverConfirmed;
    public static event GameDelegate OnGameOver;

    public static GameManager Instance;

    [SerializeField] private GameObject startPage;
    [SerializeField] private GameObject gameOverPage;
    [SerializeField] private GameObject countDownPage;
    [SerializeField] private Text scoreText;
    [SerializeField] private AudioClip audioClip;

    private int score = 0;
    private bool gameOver = true;
    private AudioSource audioSource;

    public bool GameOver { get => gameOver; }

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        Instance = this;
    }

    private void SetPageState(PageState state)
    {
        switch (state)
        {
            case PageState.None:
                startPage.SetActive(false);
                gameOverPage.SetActive(false);
                countDownPage.SetActive(false);
                break;
            case PageState.Start:
                startPage.SetActive(true);
                gameOverPage.SetActive(false);
                countDownPage.SetActive(false);
                break;
            case PageState.GameOver:
                startPage.SetActive(false);
                gameOverPage.SetActive(true);
                countDownPage.SetActive(false);
                break;
            case PageState.Countdown:
                startPage.SetActive(false);
                gameOverPage.SetActive(false);
                countDownPage.SetActive(true);
                break;
            default:
                break;
        }
    }

    private void OnEnable()
    {
        CountDownText.OnCountDownFinished += OnCountDownFinished;
        TapController.OnPlayerDied += OnPlayerDied;
        TapController.OnPlayerScored += OnPlayerScored;
    }

    private void OnDisable()
    {
        CountDownText.OnCountDownFinished -= OnCountDownFinished;
        TapController.OnPlayerDied -= OnPlayerDied;
        TapController.OnPlayerScored -= OnPlayerScored;
    }

    private void OnCountDownFinished()
    {
        SetPageState(PageState.None);
        audioSource.PlayOneShot(audioClip);
        OnGameStarted(); // event sent to TapController
        score = 0;
        gameOver = false;
    }

    private void OnPlayerScored()
    {
        score++;
        scoreText.text = score.ToString();
    }

    private void OnPlayerDied()
    {
        gameOver = true;
        int savedScore = PlayerPrefs.GetInt("HighScore");
        if (score > savedScore)
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
        OnGameOver(); // send an event to BGMusic
        SetPageState(PageState.GameOver);
    }

    public void ConfirmGameOver()
    {
        /// TODO activated when replay button is hit
        OnGameOverConfirmed(); // event sent to TapController
        scoreText.text = "0";
        SetPageState(PageState.Start);
    }

    public void StartGame()
    {
        /// TODO activated when play button is hit
        SetPageState(PageState.Countdown);
    }
}
