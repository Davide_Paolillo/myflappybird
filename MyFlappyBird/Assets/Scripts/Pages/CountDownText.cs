﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class CountDownText : MonoBehaviour
{
    public delegate void CountDownFinished();
    public static event CountDownFinished OnCountDownFinished;

    [SerializeField] private AudioClip countDownClip;

    private Text countdown;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Chiamata quando questo oggetto viene chiamato e diventa attivo
    private void OnEnable()
    {
        countdown = GetComponent<Text>();
        countdown.text = "3";
        StartCoroutine("Countdown");
    }

    private IEnumerator Countdown()
    {
        int count = 3;
        for(int i = 0; i < count; i++)
        {
            countdown.text = (count - i).ToString();
            yield return new WaitForSeconds(1);
            audioSource.PlayOneShot(countDownClip);
        }
        OnCountDownFinished(); // Call al GameManager tramite evento
    }
}
